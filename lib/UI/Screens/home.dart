import 'package:flutter/material.dart';
import 'package:todo/Logic/controllers.dart';
import 'package:get/get.dart';
import 'package:todo/UI/Screens/search.dart';
import 'package:todo/UI/Widgets/TaskView.dart';

class HomeScreen extends StatelessWidget {

  static final TaskController controller = Get.put(TaskController());

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('ToDo'),
        leading: Builder(
          builder: (context) {
            return IconButton(
              onPressed: (){
                Scaffold.of(context).openDrawer();
              },
              icon: Icon(Icons.list),
            );
          }
        ),
        actions: [
          IconButton(
              onPressed: (){
                showSearch(
                    context: context,
                    delegate: MyDelegate()
                );
              },
              icon: Icon(Icons.search),
          )
        ],
      ),
      body: GetBuilder<TaskController>(
        init: controller,
        builder: (controller) {
          return ListView.separated(
              itemCount: controller.tasks.length,

              itemBuilder: (context,i) =>
                  TaskView(controller.tasks[i]),

            separatorBuilder: (context,i) => Divider(
              color: Colors.black,
            ),
          );
        }
      ),
      drawer: Drawer(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_task),
        onPressed: (){
          Navigator.pushNamed(context, '/add');
        },
      ),
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

    );
  }
}
