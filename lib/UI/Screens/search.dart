import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo/Logic/controllers.dart';
import 'package:todo/UI/Widgets/TaskView.dart';

import '../../Logic/models.dart';

class MyDelegate extends SearchDelegate{


  @override
  Widget buildSuggestions(BuildContext context) {

    TaskController controller = Get.find();
    List<TaskModel> result = controller.search(query);

    return ListView.builder(
      itemCount: result.length,
      itemBuilder: (context,i) =>
        ListTile(
          title: Text(result[i].title),
          onTap: (){
            query = result[i].title;
          },
        ),
    );

  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: (){
            query = '';
          },
          icon: Icon(Icons.delete_forever)
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        onPressed: (){
          Navigator.pop(context);
        },
        icon: Icon(Icons.close)
    );
  }

  @override
  Widget buildResults(BuildContext context) {

    TaskController controller = Get.find();
    List<TaskModel> result = controller.search(query);

    return ListView.builder(
        itemCount: result.length,
        itemBuilder: (context,i) => TaskView(result[i])
    );
  }



}