import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo/Logic/controllers.dart';
import 'package:todo/Logic/models.dart';


class TaskView extends StatelessWidget {

  final TaskModel model;


  TaskView(this.model);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
        title: Text(model.title),
        subtitle: Text('${model.date.year} - ${model.date.month} - ${model.date.day}'),
        leading: Icon(Icons.task),
        children: [
          ListTile(
            title: Text(model.description),
          ),
          ElevatedButton(
              onPressed: (){
                TaskController controller = Get.find();
                controller.remove(model);
              },
              child: Text('remove')
          )
        ],
    );
  }
}
