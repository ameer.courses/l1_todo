import 'package:flutter/material.dart';
import 'package:todo/UI/Screens/add.dart';
import 'UI/Screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/home' : (c) => HomeScreen(),
        '/add' : (c) => AddScreen()
      },

      initialRoute: '/home',
    );
  }
}
